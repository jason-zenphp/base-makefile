# Base Makefile and Drupal Environment Setup

## Prerequisites
* ~~User has SSH key access to BitBucket configured~~ (coming soon)
* Database and db user exist
* DNS is updated
    As an alternative, you can update your workstation's host file as an interim solution, but in order to collaboratively work on the site, the DNS needs to be updated.:

## Setup and Use
1. Clone this repository to your home directory on Viper/Droplet for use, checkout the **stable** branch.
2. From within the repository root, enter the command `php composer.phar install` to install the support tooling
3. Once all the items are downloaded and installed, run the command `vendor/bin/phing bootstrap`
    * You will be asked what the project root is (the default is /var/www, which is viper's location)
    * You will be asked what folder name to create for the project inside of the project root
    * The folder structure will be created, and the build tools and makefile will be transferred to this location.
4. `cd` to the new project root folder (e.x. /var/www/sandbox)
5. Edit the dev.build.properties file to set the paths and db connection information.
    *Note* you can copy this file and name it \<something\>.build.properties for use in another environment.
6. Run the command `vendor/bin/phing site-init`
    * You will be asked which environment file you wish to use. (Default is dev)
    *Note* Whatever value you give here must have a \<env\>.build.properties file.
    * The settings file will be symlinked to build.properties.
    * `drush make` will be invoked on the makefile
    * Once `drush make` is complete `drush site-install` will be invoked on the site
7. Once complete, set up the Nginx/Apache Vhost configuration for the site
   *Note* This cannot be done in advance because the path to the document root does not exist until the _site-init_ (step 6) command is complete
8. The site is now ready for use.

## Cleanup and Final Steps

Nothing here is strictly required to begin working, except maybe the ownership and permission cleanup, but are recommended as best practices.

1. Change directory to the Drupal root (e.g. cd htdocs)
2. Run `drush zen <project_name>` (e.x. `drush zen college_bound`) to generate a bare zen subtheme for the site.
3. Change to the project root (e.g. /var/www/<projectname>) directory.
4. Dump the initial database by running the command `vendor/bin/phing db-dump`.  Accept the default value of 'master' for branch.
5. Initialize the site as a git repository by running `git init .` (Don't forget the '.' at the end)
6. Stage everything to be committed by running `git add . --all`
7. Run `git commit -am "Initial Import"` to commit everything.
8. Create a new repository on bitbucket and follow the directions for importing an existing repository.
9. Cleanup the file ownership by running `sudo chown root:osteam -R /var/www/<projectname>`
10. Cleanup the file permissions by running `sudo chown -R ug+w /var/www/<projectname>`
