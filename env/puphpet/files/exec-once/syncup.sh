#!/bin/bash
/usr/bin/rsync -rKz /home/vagrant/site/ /var/www/sandbox/
/bin/chown -R www-data:www-data /var/www/
/bin/chmod -R ug+w /var/www
usermod -aG vagrant www-data
usermod -aG www-data vagrant



