#!/bin/bash

pear channel-discover pear.symfony.com
pear channel-discover pear.netpirates.net
pear channel-discover pear.pdepend.org
pear channel-discover pear.phpmd.org
pear channel-discover pear.phpdoc.org
pear channel-discover pear.phpunit.de
pear channel-discover pear.phing.info

pear install -s phing/phing
pear install -s channel://pear.phpunit.de/PHPUnit
pear install -s channel://pear.phpunit.de/PHP_CodeCoverage
pear install -s channel://pear.phpmd.org/PHP_PMD
pear install -s channel://pear.phpunit.de/phpcpd
pear install -s channel://pear.phpunit.de/phploc
pear install -s pear/PHP_CodeSniffer
pear install -s channel://pear.php.net/VersionControl_Git-0.4.4
pear install -s channel://pear.phpdoc.org/phpDocumentor
